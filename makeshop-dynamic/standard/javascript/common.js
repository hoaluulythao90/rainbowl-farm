
$(document).ready(function () {

	$(".contactClick").on('click', function () {
    $(".pageContact").addClass('model-open');
    $(".rainbow").addClass('hidden');
  });
  $(".close, .bg-overlay").click(function () {
    $(".pageContact").removeClass('model-open');
    $(".rainbow").removeClass('hidden');
  });

	$('#nav-icon3').click(function () {
		$(this).toggleClass('open');
		$(".navList").toggleClass('open');
		$(".headerIshare").toggleClass('fixed');
		$("body").toggleClass('js-mobile-menu-open');
	});

	//menu
	$(".js__hamburger").on("click", function () {
		$(this).toggleClass("active");
		$('.hidePC').slideToggle("slow");
		$('.menuSP').hide();
		$('.navList__item .contentSP').removeClass('active');
		$('.rainbow').toggleClass("hiddenSP");
		$(".navList__item .contentSP").removeClass('trans');
	});

	$(".menuWrapper").on("click", function () {
		$('.menu').hide();
		$(this).toggleClass('active');
		$('.navList .content').removeClass("active");
		$('.rainbow').toggleClass("hidden");
	});
	//  var mapon = 0;
	$(".navList .content").on("click", function () {

		if ($(this).hasClass("active")) {
			$(this).removeClass('active');
			$('.menuWrapper').removeClass('active');
			$('.rainbow').removeClass("hidden");
			$('.header .menu.' + $(this).attr('id')).slideUp('slow');
		} else {
			$(".headerCnt__nav .navList .content").removeClass("active");
			$(this).addClass('active');
			$('.menuWrapper').addClass('active');
			$('.rainbow').addClass("hidden");
			$('.header .menu').css('display', "none");
			$('.header .menu.' + $(this).attr('id')).slideDown('slow');
		}
	});


	$(".itemSP .contentSP__wrapper").on("click", function () {
		$(this).parents('.itemSP').find(".menuSP").slideToggle('slow');
		$(this).parents('.itemSP').find(".navLinks").toggleClass('active');
		$(this).parents('.itemSP').find(".navLinks").toggleClass('trans');
		// $('.menuSmall').hide();
		$(this).parents('.navList__item').find(".menuSmall").hide();
		$('.menuList .links').removeClass("activeMenuSm");
	});


	//Active menu
	$(function () {
		var current = location.pathname.split("/")[1];

		$('.navList .navList__item .navLinks').each(function () {
			var $this = $(this);
			if (($this.attr('href').indexOf(current) !== -1) && current !== "") {
				$this.addClass('active');
			}
		})
		$('.navList .itemSP .navLinks').each(function () {
			var $this = $(this);
			if (($this.attr('href').indexOf(current) !== -1) && current !== "") {
				$this.parents('.itemSP').find('.contentSP__wrapper').addClass('active');
			}
		})
	})
	var href = location.pathname.split("/")[1];
	if (href == "keto" || href == "corporate" || href == "company" || href == "history" || href == "social" || href == "recruitment") {
		$('[href*="../about/"]').parents('.itemSP').find('.contentSP__wrapper').addClass('active');
		$('#menu1').find('.navLinks').addClass('active');
	}
	if (href == "case-study" || href == "column" || href == "materials") {
		$('[href*="../cases-columns-materials/"]').parents('.itemSP').find('.contentSP__wrapper').addClass('active');
		$('#menu2').find('.navLinks').addClass('active');
	}
	if (href == "products-services") {
		$('[href*="../products-services/"]').parents('.itemSP').find('.contentSP__wrapper').addClass('active');
		$('#menu1').find('.navLinks').addClass('active');
	}
	console.log(href);



	//slider
	$('.newSlider').slick({
		lazyLoad: 'ondemand',
		slidesToShow: 4,
		slidesToScroll: 1,
		prevArrow: '<p class="btnSlider prev"></p>',
		nextArrow: '<p class="btnSlider next"></p>',
		autoplay: false,
			autoplaySpeed: 2000,
		responsive: [
		{
			breakpoint: 1279,
			settings: {
				slidesToShow: 3,
				slidesToScroll: 1,
			}
			},
		{
		breakpoint: 1025,
		settings: {
			slidesToShow: 2,
			slidesToScroll: 1,
		}
		},
		{
		breakpoint: 576,
		settings: {
			slidesToShow: 1,
			slidesToScroll: 1
		}
		}
		]
	});

	/******************************
		Auto load hashtag
	 *****************************/
	jQuery(window).on("load", function () {
		var urlHash = window.location.href.split("#")[1];
		if (urlHash && jQuery('#' + urlHash).length) {
			scrollHasTag('#' + urlHash);
		};
	});


	// Back to top
	//Click event to scroll to top
	// $('.scrollToTop').click(function () {
	// 	$('html, body').animate({ scrollTop: 0 }, 500);
	// 	return false;
	// });

	$('button[type="submit"]').click(function () {
		$('.mySelect').removeAttr('required')
	});
});

$(".menuList .links").click(function () {
	$(this).toggleClass("activeMenuSm");
	$(this).parents(".menuList__item").find(".menuSmall").slideToggle("slow");
});

function ssl_login(type, etc, user_id){
	var etc = (typeof(etc) != "undefined") ? etc : '';
	var user_id = (typeof(user_id) != "undefined") ? user_id : '';

	if (typeof(ga) === 'function') { ga('linker:decorate', document.ssl_login_form); }

	document.ssl_login_form.type.value = type;
	document.ssl_login_form.opt.value = etc;
	document.ssl_login_form.user_id.value = user_id;
	document.ssl_login_form.submit();
}

$(function() {
	$('.spinner_down').on('click', function() {
			var index = $('.spinner_down').index(this);
			spinner('down', index);
	});
	
	$('.spinner_up').on('click', function() {
			var index = $('.spinner_up').index(this);
			spinner('up', index);
	});
});

function spinner(counter, index){
	var step = 1;
	var min = 1;
	var max = 100;

	var quantity = $('.quantity').eq(index).val();
	quantity = parseInt(quantity);
	if (counter == 'up') { 
			quantity += step; 
	} else if (counter == 'down') {
			quantity -= step;
	};

	if (quantity < min) {
			quantity = min;
	};
	if (max < quantity) {
			quantity = max;
	};

	$('.quantity').eq(index).val(quantity);
	$('.quantity-change-btn').eq(index).click();
}